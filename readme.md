# RESTful API dengan Express, Sequelize, dan ejs

Project ini dibuat menggunakan NodeJS dan dengan memanfaatkan beberapa module seperti Express, Sequelize, ImageKit, dan EJS untuk View Engine

![alt text](./public/image/db-diagram.png)


## Install dependency

```bash
# Pengguna NPM
npm install

```

## run sequelize

```
sequelize db:create
sequelize db:migrate
sequelize db:seed:all
```

## Jalankan server

```
npm run dev
```
## Database Diagram
![alt text](./public/image/db-diagram.png)